import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { HttpClient } from '@angular/common/http';

/*
  Generated class for the ApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApiProvider {
  films:any=[];api_key:any;getLoginData:any=[];getInvoices:any=[];getallSigners:any=[];userData:any=[];newregisterUser:any=[];createInvoice:any=[];
  constructor(public http: Http, public httpClient: HttpClient) {
    console.log('Hello ApiProvider Provider');
  }

  getLogin(email,password) {
  	 let params = {'email':email,
	               'password':password,
	               'deviceType': 'android',
          		   'oneSignalId' : '1245',
                 'deviceToken' : '1245'
	               }
    	this.getLoginData = this.httpClient.post('http://demo.quanram.com/mobile-app/login',params)
	    return this.getLoginData
  }

  allInvoices(token){
  	this.getInvoices = this.httpClient.get('http://demo.quanram.com/mobile-app/fetch-myinvoices?api_key='+token)
    return this.getInvoices;  
  }

  usersData(token){
    console.log(token);
    this.userData = this.httpClient.get('http://demo.quanram.com/mobile-app/fetch-user-data?api_key='+token)
    return this.userData;
  }

  allSigners(token){
    this.getallSigners = this.httpClient.get('http://demo.quanram.com/mobile-app/fetch-all-signers?api_key='+token)
    return this.getallSigners;
  }

  newcreateInvoice(token,payee,payer,amount_numbers,amount_words,no_of_days,start_payment_date,end_payment_date,buyer_name,buyers_email,buyer_address,bill_no,bill_date,buyers_place,supplier_place,filename){

    let params = {'api_key':token,
                 'payee':payee,
                 'payer': payer,
                 'amount_numbers' : amount_numbers,
                 'amount_words' : amount_words,
                 'no_of_days':no_of_days,
                 'start_payment_date':start_payment_date,
                 'end_payment_date': end_payment_date,
                 'buyer_name' : buyer_name,
                 'buyers_email' : buyers_email,
                 'buyer_address':buyer_address,
                 'bill_no':bill_no,
                 'bill_date': bill_date,
                 'buyers_place' : buyers_place,
                 'supplier_place' : supplier_place,
                 'filename' : filename

                 }
      this.createInvoice = this.httpClient.post('http://demo.quanram.com/mobile-app/create-invoice',params)
      return this.createInvoice;
  }

  registerUser(name,email,password,mobNo,gst_no,tin_no,tan_no,pan_no,city,com_aadhar){
    let params = {'name':name,
                 'email':email,
                 'password': password,
                 'role' : 'supplier',
                 'mobNo' : mobNo,
                 'gst_no' : gst_no,
                 'tin_no' : tin_no,
                 'tan_no' : tan_no,
                 'pan_no' : pan_no,
                 'city' : city,
                 'com_aadhar' : com_aadhar
                 }
      this.newregisterUser = this.httpClient.post('http://demo.quanram.com/register',params)
      return this.newregisterUser;
  }

}
