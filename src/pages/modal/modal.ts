import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ApiProvider } from './../../providers/api/api';

/**
 * Generated class for the ModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal',
  templateUrl: 'modal.html',
})
export class ModalPage {
  step2: boolean = false;
  step3button: boolean = false;
  step3: boolean = false;
  payee:any;
  payer:any;
  amount_numbers:any;
  amount_words:any;
  no_of_days:any;
  start_payment_date:any;
  end_payment_date:any;
  buyer_name:any;
  buyers_email:any;
  buyer_address:any;
  bill_no:any;
  bill_date:any;
  buyers_place:any;
  supplier_place:any;
  filename:any;
  createNewInvoice:any=[];
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl : ViewController, public forgotCtrl: AlertController, public loadingCtrl: LoadingController, private storage: Storage,public apiProvider: ApiProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalPage');
  }

  public closeModal(){
    this.viewCtrl.dismiss();
  }
  createInvoice(){
    if(this.payee != null && this.payer != null && this.amount_numbers != null && this.amount_words != null && this.no_of_days != null && this.start_payment_date != null && this.end_payment_date != null && this.buyer_name != null && this.buyers_email != null && this.buyer_address != null && this.bill_no != null && this.bill_date != null && this.buyers_place != null && this.supplier_place){
        this.storage.get('token').then((val) => {
          if (val != null) {
                let loading = this.loadingCtrl.create({content : "Please wait..."});
               loading.present(); 
                this.createNewInvoice = this.apiProvider.newcreateInvoice(val,this.payee,this.payer,this.amount_numbers,this.amount_words,this.no_of_days, this.start_payment_date,this.end_payment_date, this.buyer_name,this.buyers_email,this.buyer_address,this.bill_no,this.bill_date,this.buyers_place,this.supplier_place,this.filename)
                 .subscribe(data => {
                  loading.dismissAll();
                   this.viewCtrl.dismiss();
                  if(data.status == 'success'){
                    let alert = this.forgotCtrl.create({
                      title: 'Success!',
                      subTitle: 'Invoice Created Successfully.',
                      buttons: ['OK']
                    });
                    alert.present();
                  }else{
                    this.viewCtrl.dismiss();
                    let alert = this.forgotCtrl.create({
                      title: 'Failed!',
                      subTitle: 'Invoice not created, please try again later.',
                      buttons: ['OK']
                    });
                    alert.present();
                  }
                },
                err => {
                  loading.dismissAll();
                  console.log("ERROR!: ", err);
                  console.log(JSON.stringify(err));
                });
          } else {
            
          }
        }).catch((err) => {
          console.log(err)
        });
    }else{
          let alert = this.forgotCtrl.create({
            title: 'Failed!',
            subTitle: 'Please fill the form.',
            buttons: ['OK']
          });
          alert.present();
    }
  }

  onStepTwo() {
     this.step2 = true;
     this.step3button = true;
  }

  onStepThree() {
     this.step3 = true;
     this.step3button = false;
  }

}
