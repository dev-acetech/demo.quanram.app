import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateinvoicesPage } from './createinvoices';

@NgModule({
  declarations: [
    CreateinvoicesPage,
  ],
  imports: [
    IonicPageModule.forChild(CreateinvoicesPage),
  ],
})
export class CreateinvoicesPageModule {}
