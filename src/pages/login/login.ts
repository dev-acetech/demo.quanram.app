import { ApiProvider } from './../../providers/api/api';
import {Component} from "@angular/core";
import {NavController, AlertController, ToastController, MenuController, LoadingController} from "ionic-angular";
import {HomePage} from "../home/home";
import {RegisterPage} from "../register/register";
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
    films: Observable<any>;
email:any;
password:any;
getLoginData:any=[];
  constructor(public nav: NavController, public forgotCtrl: AlertController, public menu: MenuController, public toastCtrl: ToastController, public httpClient: HttpClient, public apiProvider: ApiProvider, private storage: Storage, public loadingCtrl: LoadingController) {
    this.menu.swipeEnable(false);
    
  }

  //logindata = {}
  logForm() {
    //console.log(this.logindata)
  }
  // go to register page
  register() {
    this.nav.setRoot(RegisterPage);
  }

  // login and go to home page
  login() {
    if(this.email != null && this.password != null){
      let loading = this.loadingCtrl.create({content : "Please wait..."});
      loading.present(); 
          this.getLoginData = this.apiProvider.getLogin(this.email,this.password)
          .subscribe(data => {
              if(data.status == 'success'){
                 this.storage.set('token',data.token);
                  setTimeout( () => {
                    loading.dismissAll();
                      this.nav.setRoot(HomePage, {}, {animate: true, direction: 'forward'});
                 }, 1000);
              }else{
                  loading.dismissAll();
                  let alert = this.forgotCtrl.create({
                    title: 'Failed!',
                    subTitle: 'Sorry, Incorrect Username/Password.',
                    buttons: ['OK']
                  });
                  alert.present();
              }
            },
            err => {
                loading.dismissAll();
                  let alert = this.forgotCtrl.create({
                    title: 'Failed!',
                    subTitle: 'Sorry, Something went wrong, please try again later.',
                    buttons: ['OK']
                  });
                  alert.present();
              console.log("ERROR!: ", err);
              console.log(JSON.stringify(err));
            });
          console.log(this.getLoginData);
    }else{
      let alert = this.forgotCtrl.create({
          title: 'Failed!',
          subTitle: 'Please fill the form.',
          buttons: ['OK']
        });
        alert.present();
    }
   


    //this.nav.setRoot(HomePage);
  }

  forgotPass() {
    let forgot = this.forgotCtrl.create({
      title: 'Forgot Password?',
      message: "Enter you email address to send a reset link password.",
      inputs: [
        {
          name: 'email',
          placeholder: 'Email',
          type: 'email'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Send',
          handler: data => {
            console.log('Send clicked');
            let toast = this.toastCtrl.create({
              message: 'Email was sended successfully',
              duration: 3000,
              position: 'top',
              cssClass: 'dark-trans',
              closeButtonText: 'OK',
              showCloseButton: true
            });
            toast.present();
          }
        }
      ]
    });
    forgot.present();
  }

}
