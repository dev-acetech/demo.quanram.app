import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ApiProvider } from './../../providers/api/api';

/**
 * Generated class for the SignersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signers',
  templateUrl: 'signers.html',
})
export class SignersPage {
	getAllSigners:any=[];
  constructor(public navCtrl: NavController, public navParams: NavParams,private storage: Storage,
    public apiProvider: ApiProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignersPage');
    this.getAllSigner();
  }

  getAllSigner(){
  	this.storage.get('token').then((val) => {
        if (val != null) {
              this.getAllSigners = this.apiProvider.allSigners(val)
               .subscribe(data => {
               	console.log(data);
                if(data.status == 'success' && data.invoices != 'noSigners'){
                  this.getAllSigners = data.invoices;
                }else{
                	this.getAllSigners = [];
                }
               	console.log(this.getAllSigners);
                },
                err => {
                  console.log("ERROR!: ", err);
                  console.log(JSON.stringify(err));
                });
        } else {
          
        }
      }).catch((err) => {
        console.log(err)
      });
  }

}
