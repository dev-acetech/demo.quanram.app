import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SignersPage } from './signers';

@NgModule({
  declarations: [
    SignersPage,
  ],
  imports: [
    IonicPageModule.forChild(SignersPage),
  ],
})
export class SignersPageModule {}
