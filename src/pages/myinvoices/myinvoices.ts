import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import {Storage} from '@ionic/storage';
import { ApiProvider } from './../../providers/api/api';
import { Observable } from 'rxjs/Observable';
import { ModalController } from 'ionic-angular';
import {CreateinvoicesPage} from "../createinvoices/createinvoices";

/**
 * Generated class for the MyinvoicesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-myinvoices',
  templateUrl: 'myinvoices.html',
})
export class MyinvoicesPage {
  getInvoicesData:any=[];allInvoices:any=[];no_data:any=7; buttonClicked: boolean = false;
  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage, public forgotCtrl: AlertController, public loadingCtrl: LoadingController, public apiProvider: ApiProvider, public modalCtrl : ModalController) {
  }

  public openModal(){
  	var modalPage = this.modalCtrl.create('ModalPage'); modalPage.present();
	}

  ionViewWillEnter(){
  	
  }
  ionViewDidLoad() {
		this.getInvoicesFromServer();
  	
    console.log('ionViewDidLoad MyinvoicesPage');
  }
	getInvoicesFromServer(){
		this.storage.get('token').then((val) => {
      if (val != null) {
          let loading = this.loadingCtrl.create({content : "Please wait..."});
	      loading.present(); 
	          this.getInvoicesData = this.apiProvider.allInvoices(val)
	          .subscribe(data => {
	          	console.log(data);
	              if(data.status == 'success' && data.invoices != 'noInvoices'){
									this.no_data = 5;
									loading.dismissAll();
									console.log(this.no_data);
	              	this.allInvoices = data.invoices;
	              }else if(data.status == 'success' && data.invoices == 'noInvoices'){
										this.no_data = 1;
										console.log(this.no_data);
	                  loading.dismissAll();
	              }
	            },
	            err => {
								this.no_data = 1;
	                loading.dismissAll();
	                  let alert = this.forgotCtrl.create({
	                    title: 'Failed!',
	                    subTitle: 'Sorry, Something went wrong, please try again later.',
	                    buttons: ['OK']
	                  });
	                  alert.present();
	              console.log("ERROR!: ", err);
	              console.log(JSON.stringify(err));
	            });
      } else {
        
      }
    }).catch((err) => {
      console.log(err)
    });
	}

	onButtonClick() {
     this.buttonClicked = !this.buttonClicked;
	}
}
