import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyinvoicesPage } from './myinvoices';

@NgModule({
  declarations: [
    MyinvoicesPage,
  ],
  imports: [
    IonicPageModule.forChild(MyinvoicesPage),
  ],
})
export class MyinvoicesPageModule {}
