import {Component} from "@angular/core";
import {NavController, AlertController, LoadingController} from "ionic-angular";
import { Storage } from '@ionic/storage';
import { ApiProvider } from './../../providers/api/api';
import {LoginPage} from "../login/login";
import {HomePage} from "../home/home";


@Component({
  selector: 'page-register',
  templateUrl: 'register.html'
})
export class RegisterPage {
  step2: boolean = false;
  step3button: boolean = false;
  step3: boolean = false;
  name:any;email:any;password:any;mobNo:any;gst_no:any;tin_no:any;tan_no:any;pan_no:any;city:any;com_aadhar:any;
  constructor(public nav: NavController, public apiProvider: ApiProvider, private storage: Storage, public forgotCtrl: AlertController, public loadingCtrl: LoadingController) {
  }

  // register and go to home page
  register() {
    if(this.name != null && this.email != null && this.password != null && this.mobNo != null && this.gst_no != null && this.tin_no != null && this.tan_no != null && this.pan_no != null && this.city != null && this.com_aadhar != null){
      let loading = this.loadingCtrl.create({content : "Please wait..."});
      loading.present(); 
          this.getLoginData = this.apiProvider.registerUser(this.name, this.email, this.password, this.mobNo, this.gst_no, this.tin_no, this.tan_no, this.pan_no, this.city, this.com_aadhar)
          .subscribe(data => {
              if(data.status == 'success'){
                 this.storage.set('token',data.token);
                  setTimeout( () => {
                    loading.dismissAll();
                      this.nav.setRoot(HomePage, {}, {animate: true, direction: 'forward'});
                 }, 1000);
              }else{
                  loading.dismissAll();
                  let alert = this.forgotCtrl.create({
                    title: 'Failed!',
                    subTitle: 'Sorry, something went wrong.',
                    buttons: ['OK']
                  });
                  alert.present();
              }
            },
            err => {
                loading.dismissAll();
                  let alert = this.forgotCtrl.create({
                    title: 'Failed!',
                    subTitle: 'Sorry, Something went wrong, please try again later.',
                    buttons: ['OK']
                  });
                  alert.present();
              console.log("ERROR!: ", err);
              console.log(JSON.stringify(err));
            });
          console.log(this.getLoginData);
    }else{
              let alert = this.forgotCtrl.create({
                title: 'Failed!',
                subTitle: 'Please fill the form.',
                buttons: ['OK']
              });
              alert.present();
     
    }
  }

  // go to login page
  login() {
    this.nav.setRoot(LoginPage);
  }

  onStepTwo() {
     this.step2 = true;
     this.step3button = true;
  }

  onStepThree() {
     this.step3 = true;
     this.step3button = false;
  }

}
