import {Component} from "@angular/core";
import {NavController, PopoverController} from "ionic-angular";
import {Storage} from '@ionic/storage';

import {NotificationsPage} from "../notifications/notifications";
import {SettingsPage} from "../settings/settings";
import {TripsPage} from "../trips/trips";
import {SearchLocationPage} from "../search-location/search-location";

import { ScrollableTabs } from '../../components/scrollable-tabs/scrollable-tabs';
import {MyinvoicesPage} from "../myinvoices/myinvoices";
import {MynotificationsPage} from "../mynotifications/mynotifications";
import {CreateinvoicesPage} from "../createinvoices/createinvoices";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {
  // search condition
  public search = {
    name: "Rio de Janeiro, Brazil",
    date: new Date().toISOString()
  }
  CreateInvoicesRoot: any = CreateinvoicesPage;
  MyInvoicesPageRoot: any = MyinvoicesPage;
  MyNotificationsPageRoot: any = MynotificationsPage;
  

  tabsColor: string = "default";
  tabsMode: string = "md";
  tabsPlacement: string = "top";
  tabToShow: Array<boolean> = [false, true, true];
  scrollableTabsopts: any = {MyinvoicesPage:true,MynotificationsPage:true};

  constructor(private storage: Storage, public nav: NavController, public popoverCtrl: PopoverController) {
  }
refreshScrollbarTabs() {
    this.scrollableTabsopts = { refresh: true };    
  }

  
  ionViewWillEnter() {
    // this.search.pickup = "Rio de Janeiro, Brazil";
    // this.search.dropOff = "Same as pickup";
    this.storage.get('pickup').then((val) => {
      if (val === null) {
        this.search.name = "Rio de Janeiro, Brazil"
      } else {
        this.search.name = val;
      }
    }).catch((err) => {
      console.log(err)
    });
  }

  // go to result page
  doSearch() {
    this.nav.push(TripsPage);
  }

  // choose place
  choosePlace(from) {
    this.nav.push(SearchLocationPage, from);
  }

  // to go account page
  goToAccount() {
    this.nav.push(SettingsPage);
  }

  presentNotifications(myEvent) {
    console.log(myEvent);
    let popover = this.popoverCtrl.create(NotificationsPage);
    popover.present({
      ev: myEvent
    });
  }

}

//
